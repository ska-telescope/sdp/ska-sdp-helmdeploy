""" SDP Helm deployer Monitoring tests """

import json
import logging
from typing import Callable, Iterator
from unittest.mock import MagicMock, Mock, patch

import pytest
from kubernetes import client as k8s_client
from kubernetes.client.models.v1_deployment import V1Deployment
from kubernetes.client.models.v1_deployment_list import V1DeploymentList
from kubernetes.client.models.v1_object_meta import V1ObjectMeta
from kubernetes.client.models.v1_owner_reference import V1OwnerReference
from kubernetes.client.models.v1_pod import V1Pod
from kubernetes.client.models.v1_pod_status import V1PodStatus
from kubernetes.client.models.v1_replica_set import V1ReplicaSet
from kubernetes.client.models.v1_replica_set_list import V1ReplicaSetList
from ska_sdp_config import Config, Deployment

from ska_sdp_helmdeploy.monitoring import (
    ADDED,
    CANCELLED,
    FAILED,
    FINISHED,
    MODIFIED,
    PENDING,
    RUNNING,
    UNSET,
    _monitor_network_attachment,
    get_pod_helm_name,
    monitor_network_attach_def,
    process_k8s_network_attach_def_event,
    process_k8s_pod_event,
)


@pytest.fixture(name="config")
def config_fixture() -> Iterator[Config]:
    """A memory-backend Config object with a cleared /deploy prefix."""
    with Config(backend="memory") as config:
        config.backend.delete("/deploy", must_exist=False, recursive=True)
        config.backend.delete("/resource", must_exist=False, recursive=True)
        yield config
        config.backend.delete("/deploy", must_exist=False, recursive=True)
        config.backend.delete("/resource", must_exist=False, recursive=True)


def _create_network_attach_def(
    name: str,
    namespace: str = "namespace",
    supernet: str = "10.20.0.0/16",
    cidr_bits: int = 24,
    available: bool = True,
) -> tuple[dict, dict, dict]:
    """
    Creates a k8s NetworkAttachmentDefinition object as required by SDP with
    the given details, as well as the expected SDP resources in the config DB
    that should result from it being consumed.
    """
    k8s_network_attach_def = dict(
        {
            "kind": "NetworkAttachmentDefinition",
            "metadata": {
                "name": name,
                "namespace": namespace,
                "annotations": {
                    "sdp.skao.int/ip-supernet": supernet,
                    "sdp.skao.int/ip-subnet-cidr-bits": str(cidr_bits),
                },
                "labels": {
                    "sdp.skao.int/available-for-allocation": available,
                },
            },
        }
    )
    network_attach_def_resource = {
        "name": name,
        "namespace": namespace,
    }
    supernet_resource = {
        "ip-supernet": supernet,
        "ip-subnet-cidr-bits": cidr_bits,
    }
    return (
        k8s_network_attach_def,
        network_attach_def_resource,
        supernet_resource,
    )


def pod_event_data(
    dpl_id,
    event_state,
    pod_phase,
    condition=None,
    container_statuses=None,
    build_deploy=False,
):
    """
    Test function to return fake pod event data

    This returns an appropriate subset of data from events returned by
            for event in watch.stream(
                api_v1.list_namespaced_pod, namespace=namespace)
    param dpl_id: Database deployment_id
    param event_state: The event state to be faked for testing
    param pod_phase: The Pod phase to be faked
    param condition: Pod condition to be appended
    param container_statuses: Container statuses to be faked
    param build_deploy: Boolean flag to embed Pod into a fake deployment

    return: event as returned by K8S event stream
    """
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-arguments
    # pylint: disable=too-many-positional-arguments
    pod_metadata = V1ObjectMeta(
        name=f"{dpl_id}-pod-name-1234", namespace="testing"
    )
    pod_status = V1PodStatus(
        conditions=[{"type": "Initialized", "status": "True"}],
        phase=pod_phase,
    )

    if condition:
        pod_status.conditions.append(condition)

    if container_statuses:
        pod_status.container_statuses = container_statuses

    pod = V1Pod(metadata=pod_metadata, status=pod_status)

    # Build this into a fake deployment!

    if build_deploy:
        rs_metadata = V1ObjectMeta(name=f"{dpl_id}-rs", namespace="testing")

        replicaset = V1ReplicaSet(metadata=rs_metadata)

        dpl_metadata = V1ObjectMeta(
            name=f"{dpl_id}-dpl",
            namespace="testing",
            labels={"app.kubernetes.io/managed-by": "Helm"},
            annotations={"meta.helm.sh/release-name": f"{dpl_id}"},
        )
        dpl = V1Deployment(metadata=dpl_metadata)

        pod_owner = V1OwnerReference(
            api_version="apps/v1",
            kind="ReplicaSet",
            name=f"{dpl_id}-rs",
            uid="123",
        )
        rs_owner = V1OwnerReference(
            api_version="apps/v1",
            kind="Deployment",
            name=f"{dpl_id}-dpl",
            uid="123",
        )

        pod.metadata.owner_references = [
            pod_owner,
        ]
        replicaset.metadata.owner_references = [
            rs_owner,
        ]
        list_rs_return = V1ReplicaSetList(
            items=[
                replicaset,
            ]
        )
        list_dpl_return = V1DeploymentList(
            items=[
                dpl,
            ]
        )
        api = k8s_client.api.apps_v1_api.AppsV1Api
        api.list_namespaced_replica_set = Mock(return_value=list_rs_return)
        api.list_namespaced_deployment = Mock(return_value=list_dpl_return)
    else:
        pod.metadata.labels = {"app.kubernetes.io/managed-by": "Helm"}
        pod.metadata.annotations = {"meta.helm.sh/release-name": f"{dpl_id}"}

    return {"object": pod, "type": event_state}


def test_helm_name_matching(caplog):
    """
    Test the function which works back from a Pod through
    the Kubernetes hierarchy (eg. Pod->ReplicaSet->Deployment)
    to obtain the Helm deployment name
    """
    caplog.set_level(logging.DEBUG)
    fake_k8s_event = pod_event_data(
        "test-pod", "ADDED", "pending", build_deploy=False
    )
    pod = fake_k8s_event["object"]
    helm_name = get_pod_helm_name(pod)
    assert helm_name == "test-pod"

    caplog.clear()
    fake_k8s_event = pod_event_data(
        "test-tree", "ADDED", "pending", build_deploy=True
    )
    pod_tree = fake_k8s_event["object"]
    helm_name = get_pod_helm_name(pod_tree)
    assert caplog.record_tuples == [
        (
            "ska_sdp_helmdeploy.monitoring",
            logging.DEBUG,
            "Searching for Helm chart associated with pod "
            "test-tree-pod-name-1234",
        ),
        (
            "ska_sdp_helmdeploy.monitoring",
            logging.DEBUG,
            "Pod test-tree-pod-name-1234 hierarchy - "
            "type/name = ReplicaSet/test-tree-rs",
        ),
        (
            "ska_sdp_helmdeploy.monitoring",
            logging.DEBUG,
            "Pod test-tree-pod-name-1234 hierarchy - "
            "type/name = Deployment/test-tree-dpl",
        ),
    ]
    assert helm_name == "test-tree"


def test_helm_name_matching_no_match(caplog):
    """
    Pod event is not matched to any helm deployment.
    "helm_name" is returned as an empty string.
    """
    caplog.set_level(logging.DEBUG)
    fake_k8s_event = pod_event_data(
        "test-pod", "ADDED", "pending", build_deploy=False
    )
    pod = fake_k8s_event["object"]

    # remove annotations which say this was added by Helm
    pod.metadata.labels = {}
    pod.metadata.annotations = {}

    helm_name = get_pod_helm_name(pod)
    assert helm_name == ""
    assert (
        "Pod manager test-pod-pod-name-1234 not from Helm" in caplog.messages
    )


def test_helm_name_matching_removed_owner(caplog):
    """
    When an owner of a pod (e.g. a ReplicaSet) no longer
    exists in the searched namespace, helm_name is returned as "".
    """
    caplog.set_level(logging.DEBUG)
    fake_k8s_event = pod_event_data(
        "test-tree", "ADDED", "pending", build_deploy=True
    )
    pod_tree = fake_k8s_event["object"]

    api = k8s_client.api.apps_v1_api.AppsV1Api
    api.list_namespaced_replica_set = Mock(
        return_value=V1ReplicaSetList(items=[])
    )

    helm_name = get_pod_helm_name(pod_tree)
    assert helm_name == ""
    assert (
        "Failed to trace Pod hierarchy - No Helm name found" in caplog.messages
    )


def test_helm_name_matching_no_owner_match(caplog):
    """
    When there are "owners" in the namespace in the form of
    ReplicaSets, but none of those match the one our pod
    was created by, i.e. we can't find or pod's owner.
    helm_name is returned as "".
    """
    caplog.set_level(logging.DEBUG)
    fake_k8s_event = pod_event_data(
        "test-tree", "ADDED", "pending", build_deploy=True
    )
    pod_tree = fake_k8s_event["object"]

    # Here we add a replicaset (rs) to the namespace that does not match the
    # name of the pod -> an rs that did not create the pod, while the
    # pod's rs does not exist
    rs_metadata = V1ObjectMeta(name="random-replicaset", namespace="testing")
    replicaset = V1ReplicaSet(metadata=rs_metadata)
    list_rs_return = V1ReplicaSetList(
        items=[
            replicaset,
        ]
    )
    api = k8s_client.api.apps_v1_api.AppsV1Api
    api.list_namespaced_replica_set = Mock(return_value=list_rs_return)

    helm_name = get_pod_helm_name(pod_tree)
    assert helm_name == ""
    assert (
        "Failed to trace Pod hierarchy - No Helm name found" in caplog.messages
    )


def test_monitoring_pending_running(config):
    """
    Test helmdeploy deployment monitoring when deployment is
    PENDING or RUNNING (event_type MODIFIED or ADDED)
    """
    # creating test Helm deployments in the 'in memory'
    # Configuration database
    dpl_id = "test"
    dpl = Deployment(
        key=dpl_id,
        kind="helm",
        args={"chart": "test", "values": {"test": "test"}},
    )

    for txn in config.txn():
        # one deployment in pending state
        txn.deployment.create(dpl)
        process_k8s_pod_event(config, pod_event_data("test", ADDED, "Pending"))

        deploys = txn.deployment.list_keys()
        assert deploys == ["test"]
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["test-pod-name-1234"]["status"] == PENDING

    for txn in config.txn():
        # add another deployment, in running state but
        # pod condition is not ready
        dpl_id = "second"
        second_dpl = dpl.model_copy(deep=True)
        second_dpl.key = dpl_id
        txn.deployment.create(second_dpl)
        process_k8s_pod_event(
            config, pod_event_data("second", MODIFIED, "Running")
        )

        deploys = txn.deployment.list_keys()
        assert deploys == ["second", "test"]
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["second-pod-name-1234"]["status"] == PENDING

    for txn in config.txn():
        # second pod reaches ready condition
        process_k8s_pod_event(
            config,
            pod_event_data(
                "second",
                MODIFIED,
                "Running",
                condition={"type": "Ready", "status": "True"},
            ),
        )
        deploys = txn.deployment.list_keys()
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["second-pod-name-1234"]["status"] == RUNNING


@pytest.mark.parametrize(
    "phase, expected_status",
    [
        ("Pending", PENDING),
        ("Succeeded", FINISHED),
        ("Failed", FAILED),
        ("Unknown", UNSET),
    ],
)
def test_monitoring_phase(phase, expected_status, config):
    """
    Test helmdeploy deployment monitoring based on event phase
    (event_type MODIFIED)
    """
    # creating test Helm deployments in the 'in memory'
    # Configuration database
    dpl_id = "test"
    dpl = Deployment(
        key=dpl_id,
        kind="helm",
        args={"chart": "test", "values": {"test": "test"}},
    )

    for txn in config.txn():
        # one deployment in pending state
        txn.deployment.create(dpl)
        process_k8s_pod_event(config, pod_event_data("test", MODIFIED, phase))

        deploys = txn.deployment.list_keys()
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["test-pod-name-1234"]["status"] == expected_status


@pytest.mark.parametrize(
    "event_type, expected_status",
    [
        ("DELETED", CANCELLED),
        ("FAILED", FAILED),
    ],
)
def test_monitoring_event_type(event_type, expected_status, config):
    """
    Test helmdeploy deployment monitoring based on event type
    """
    # creating test Helm deployments in the 'in memory'
    # Configuration database
    dpl_id = "test"
    dpl = Deployment(
        key=dpl_id,
        kind="helm",
        args={"chart": "test", "values": {"test": "test"}},
    )

    for txn in config.txn():
        # one deployment in pending state
        txn.deployment.create(dpl)
        process_k8s_pod_event(config, pod_event_data("test", event_type, None))

        deploys = txn.deployment.list_keys()
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["test-pod-name-1234"]["status"] == expected_status


def test_pod_terminated(config):
    """
    Test processing of Pod abnormal termination
    """
    # creating test Helm deployment
    dpl_id = "test"
    dpl = Deployment(
        key=dpl_id,
        kind="helm",
        args={"chart": "test", "values": {"test": "test"}},
    )
    failed_container_status = [
        {
            "name": "test_container_name",
            "image": "test_image",
            "state": {
                "terminated": {
                    "reason": "some_failure",
                    "exit_code": 999,
                },
                "waiting": None,
                "running": None,
            },
        }
    ]

    for txn in config.txn():
        txn.deployment.create(dpl)

        process_k8s_pod_event(
            config,
            pod_event_data(
                "test",
                MODIFIED,
                "failed",
                container_statuses=failed_container_status,
            ),
        )

        deploys = txn.deployment.list_keys()
        assert deploys == ["test"]
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["test-pod-name-1234"]["status"] == "FAILED"
        assert state["pods"]["test-pod-name-1234"]["error_state"] == {
            "container_name": "test_container_name",
            "state": "terminated",
            "reason": "some_failure",
            "exit_code": 999,
        }


def test_pod_imagepull_failed(config):
    """
    Test processing of Pod image pull failure
    """
    # creating test Helm deployment
    dpl_id = "test"
    dpl = Deployment(
        key=dpl_id,
        kind="helm",
        args={"chart": "test", "values": {"test": "test"}},
    )
    imagepull_status = [
        {
            "name": "test_container_name",
            "image": "test_image",
            "state": {
                "waiting": {
                    "reason": "ErrImagePull",
                    "message": "Failed to pull some image file",
                },
                "running": None,
                "terminated": None,
            },
        }
    ]
    for txn in config.txn():
        txn.deployment.create(dpl)

        process_k8s_pod_event(
            config,
            pod_event_data(
                "test",
                MODIFIED,
                "pending",
                container_statuses=imagepull_status,
            ),
        )
        deploys = txn.deployment.list_keys()
        assert deploys == ["test"]
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["test-pod-name-1234"]["status"] == "FAILED"
        assert state["pods"]["test-pod-name-1234"]["error_state"] == {
            "container_name": "test_container_name",
            "state": "waiting",
            "reason": "ErrImagePull",
            "message": "Failed to pull some image file",
        }


def test_pod_containercreation(config):
    """
    Test processing of Pod waiting for Container Creation
    """
    dpl_id = "test"
    dpl = Deployment(
        key=dpl_id,
        kind="helm",
        args={"chart": "test", "values": {"test": "test"}},
    )
    create_status = [
        {
            "name": "test_container_name",
            "image": "test_image",
            "state": {
                "waiting": {
                    "reason": "ContainerCreating",
                    "message": "Pull some image file",
                },
                "running": None,
                "terminated": None,
            },
        }
    ]
    for txn in config.txn():
        txn.deployment.create(dpl)

        process_k8s_pod_event(
            config,
            pod_event_data(
                "test",
                MODIFIED,
                "pending",
                container_statuses=create_status,
            ),
        )
        deploys = txn.deployment.list_keys()
        assert deploys == ["test"]
        state = txn.deployment.state(deploys[0]).get()
        assert state["pods"]["test-pod-name-1234"]["status"] == PENDING
        assert "error_state" not in state["pods"]["test-pod-name-1234"]


def test_custom_object_added(config: Config):
    """
    Test that a properly-populated k8s NetworkAttachmentDefinition can be
    processed and results in the expected entries in the SDP config DB.
    """
    name = "name"
    (
        k8s_nad,
        sdp_nad_resource,
        sdp_supernet_resource,
    ) = _create_network_attach_def(name)
    process_k8s_network_attach_def_event(
        config, {"type": ADDED, "object": k8s_nad}
    )
    for txn in config.txn():
        assert sdp_nad_resource == json.loads(
            txn.raw.get(f"/resource/network-attachment-definition:{name}")
        )
        assert sdp_supernet_resource == json.loads(
            txn.raw.get(f"/resource/supernet:{name}")
        )


@pytest.mark.parametrize(
    "k8s_nad_modifier",
    (
        # Either make pylint happy or make these functions, chose the former
        # pylint: disable=unnecessary-dunder-call
        lambda k8s_nad: k8s_nad["metadata"].__delitem__("annotations"),
        lambda k8s_nad: k8s_nad["metadata"]["annotations"].__delitem__(
            "sdp.skao.int/ip-supernet"
        ),
        lambda k8s_nad: k8s_nad["metadata"]["annotations"].__delitem__(
            "sdp.skao.int/ip-subnet-cidr-bits"
        ),
        lambda k8s_nad: k8s_nad.__setitem__(
            "kind", "NotANetworkAttachmentDefinition"
        ),
    ),
)
def test_incomplete_custom_object_isnt_added(
    config: Config, k8s_nad_modifier: Callable[[dict], None]
):
    """
    Test that an incomplete k8s NetworkAttachmentDefinition can be processed,
    but results in no new entries in the SDP config DB.
    """

    name = "name"
    (
        k8s_nad,
        _,
        _,
    ) = _create_network_attach_def(name)
    k8s_nad_modifier(k8s_nad)
    process_k8s_network_attach_def_event(
        config, {"type": ADDED, "object": k8s_nad}
    )
    for txn in config.txn():
        assert (
            txn.raw.get(f"/resource/network-attachment-definition:{name}")
            is None
        )
        assert txn.raw.get(f"/resource/supernet:{name}") is None


@pytest.mark.parametrize(
    "http_status,expected_level,expected_message",
    [
        (410, "WARNING", "NetworkAttachmentDefinitions Gone"),
        (404, "WARNING", "NetworkAttachmentDefinitions Not Found"),
        (403, "ERROR", "NetworkAttachmentDefinitions Forbidden"),
        (401, "ERROR", "Unexpected Kubernetes API error"),
    ],
)
@patch("ska_sdp_helmdeploy.monitoring._monitor_custom_objects")
# pylint: disable=too-many-arguments
# pylint: disable=too-many-positional-arguments
def test_monitor_network_attach_def_api_error_recovery(
    mock_function,
    http_status,
    expected_level,
    expected_message,
    config,
    caplog,
):
    """

    Test helmdeploy NetworkAttachmentDefinitions monitor for recovering
    against known k8s ApiExceptions.
    """
    mock_function.side_effect = k8s_client.ApiException(http_status)
    caplog.set_level("INFO")

    with (
        patch(
            "ska_sdp_helmdeploy.monitoring.NO_NAD_FOUND_RETRY_PERIOD_SEC"
        ) as mock_no_nad_sec,
        patch(
            "ska_sdp_helmdeploy.monitoring.ERROR_RETRY_PERIOD"
        ) as mock_nad_gone_sec,
    ):
        mock_no_nad_sec.return_value = 0.1
        mock_nad_gone_sec.return_value = 0.1

        # tested function
        _monitor_network_attachment(config=config, api_client=Mock())

        assert expected_message in caplog.text
        assert (
            expected_level
            == caplog.text.split("ska_sdp_helmdeploy")[0].strip()
        )


@patch(
    "ska_sdp_helmdeploy.monitoring._monitor_network_attachment",
    Mock(side_effect=[True, False]),
)
@patch("ska_sdp_helmdeploy.monitoring.Config", MagicMock())
def test_monitor_wrapper_with_args(config):
    """
    Test for ska_sdp_helmdeploy.monitoring.monitoring_function.
    Show that if one provides the args to the wrapped function as *args,
    the code fails
    """
    with pytest.raises(TypeError):
        monitor_network_attach_def(config, MagicMock())


@patch(
    "ska_sdp_helmdeploy.monitoring._monitor_network_attachment",
    Mock(side_effect=[True, False]),
)
def test_monitor_wrapper_with_kwargs(config):
    """
    Test for ska_sdp_helmdeploy.monitoring.monitoring_function.
    Show that if one provides the args to the wrapped function as **kwargs,
    the code executes successfully.
    """
    monitor_network_attach_def(config=config, api_client=MagicMock())
