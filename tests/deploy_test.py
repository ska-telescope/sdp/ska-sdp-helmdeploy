# pylint: disable=protected-access

""" SDP Helm deployer tests """

import logging
import os
import subprocess
from unittest.mock import Mock, patch

import pytest
from ska_sdp_config import Config
from ska_sdp_config.entity import Deployment

from ska_sdp_helmdeploy import helmdeploy
from ska_sdp_helmdeploy.helmdeploy import (
    _create_deploy_state,
    _generate_helm_command_args,
    helm_invoke,
    num_pods_in_deployment,
)


def test_invoke():
    """
    invoke succeeds and returns output of command

    This should succeed because it runs `ls` in
    the directory where the test is, which means the
    test file (deploy_test.py) should always be there.
    """
    file_path = os.path.dirname(__file__)
    result = helmdeploy.invoke("ls", file_path)
    assert "deploy_test.py" in result


def test_invoke_logs_error(caplog):
    """
    invoke fails with subprocess.CalledProcessError
    and logs the error message produced by the subprocess

    Since this is a pytest test, python should be installed.
    It will fail because "no-file" is not a valid python file,
    and it doesn't exist either.
    """
    with pytest.raises(subprocess.CalledProcessError):
        with caplog.at_level(logging.ERROR):
            helmdeploy.invoke("python", "no-file")

    assert "[Errno 2] No such file or directory" in caplog.text
    assert "ERROR" in caplog.text


@patch("subprocess.run")
def test_delete(mock_run):
    """
    Test helm deployer delete command
    """
    assert helmdeploy.delete_helm("test")
    mock_run.assert_called_once()
    mock_run.side_effect = subprocess.CalledProcessError(1, "test")
    assert not helmdeploy.delete_helm("test")
    assert mock_run.call_count == 2


@patch("subprocess.run")
def test_update(mock_run):
    """
    Test helm deployer update command
    """
    helmdeploy.update_helm()
    mock_run.assert_called_once()
    mock_run.side_effect = subprocess.CalledProcessError(1, "test")
    helmdeploy.update_helm()
    assert mock_run.call_count == 2


def run_create(mock_run, config, byte_string=None):
    """
    Helm test support function
    """
    if byte_string is not None:
        exc = subprocess.CalledProcessError(1, "test")
        exc.stdout = byte_string
        mock_run.side_effect = exc

    for txn in config.txn():
        deployment = txn.deployment.get("test")
    ok_status, _ = helmdeploy.template_and_create_chart("test", deployment)

    return ok_status


@patch("subprocess.run")
@patch(
    "ska_sdp_helmdeploy.helmdeploy.num_pods_in_deployment",
    Mock(return_value=3),
)
def test_create(mock_run):
    """
    Test creating a test Helm deployment in the 'in memory'
    Configuration database
    """
    config = Config(backend="memory")

    for txn in config.txn():
        txn.deployment.create(
            Deployment(
                key="test",
                kind="helm",
                args={"chart": "test", "values": {"test": "test"}},
            )
        )
    for txn in config.txn():
        assert helmdeploy._get_deployment(txn, "test") is not None

    assert run_create(mock_run, config)
    # helm_invoke (i.e. subprocess) is called twice:
    #   once for helm template, once for helm install
    assert mock_run.call_count == 2
    assert not run_create(mock_run, config, byte_string=b"already exists")
    assert mock_run.call_count == 3
    assert not run_create(mock_run, config, byte_string=b"doesn't exist")
    assert mock_run.call_count == 4


def test_helm_chart_version():
    """
    Test that if a deployment specifies a version for the Helm Chart
    that this gets included in the generated Helm command
    """
    deployment = Deployment(
        key="proc-pb-test-20230509-00000-script",
        kind="helm",
        args={
            "chart": "dask",
            "version": "^3.2.1",
            "values": {
                "image": "some-image:0.1.0",
                "worker": {"replicas": 3},
            },
        },
    )
    with _generate_helm_command_args("test-id", deployment) as args:
        helm_args = " ".join(args)
        assert " --version ^3.2.1 " in helm_args


@patch("ska_sdp_helmdeploy.helmdeploy.invoke")
def test_list(mock_invoke):
    """
    Test helmdeployer 'list' name logic
    """
    deployments = ["test1", "test2", "test3"]
    # With prefix
    helmdeploy.HELM_RELEASE_PREFIX = "test"
    releases = ["test-" + d for d in deployments] + ["foo", "bar"]
    mock_invoke.return_value = "\n".join(releases)
    assert helmdeploy.list_helm() == deployments
    # No prefix (default)
    helmdeploy.HELM_RELEASE_PREFIX = ""
    releases = deployments
    mock_invoke.return_value = "\n".join(releases)
    assert helmdeploy.list_helm() == deployments


def test_release_name():
    """
    Test helmdeploy release name logic
    """
    # With prefix
    helmdeploy.HELM_RELEASE_PREFIX = "test"
    assert helmdeploy.release_name("test") == "test-test"
    # No prefix (default)
    helmdeploy.HELM_RELEASE_PREFIX = ""
    assert helmdeploy.release_name("test") == "test"


@patch("subprocess.run")
@patch(
    "ska_sdp_helmdeploy.monitoring.check_current_namespace_state",
    Mock(return_value=None),
)
@patch(
    "ska_sdp_helmdeploy.helmdeploy.num_pods_in_deployment",
    Mock(return_value=3),
)
def test_main(mock_run):
    """
    Test Helm deployer main function
    """
    helmdeploy.main(backend="memory")
    assert mock_run.call_count > 0


@pytest.fixture(name="set_up_helm")
def set_up_helm_fixt():
    """
    Fixture to set up helm repo for testing
    remove repo after test finished
    """
    repo_name = "test-helm"
    repo_url = (
        "https://gitlab.com/ska-telescope/sdp/"
        "ska-sdp-helmdeploy-charts/-/raw/master/chart-repo"
    )
    helmdeploy.HELM_CHART_PREFIX = ""
    helmdeploy.HELM_CHART_REPO_NAME = repo_name

    helm_invoke("repo", "add", repo_name, repo_url)

    yield

    helm_invoke("repo", "remove", repo_name)


# pylint: disable=unused-argument
def test_num_pods_in_deployment_dask(set_up_helm):
    """
    Given a dask deployment with two worker replicas,
    we expect the function to return 4 for the number
    of pods in the deployment (1 scheduler, 3 workers).
    This is allowed by the existing 'dask' chart.
    """
    deployment = Deployment(
        key="proc-pb-test-20230509-00000-script",
        kind="helm",
        args={
            "chart": "dask",
            "values": {
                "image": "some-image:0.1.0",
                "worker": {"replicas": 3},
            },
        },
    )
    with _generate_helm_command_args("test-id", deployment) as args:
        template_string = helm_invoke("template", *args)

    num_pods = num_pods_in_deployment("test-id", template_string)

    assert num_pods == 4


def test_num_pods_in_deployment_script(set_up_helm):
    """
    The script chart runs a Job, which doesn't have
    the "replicas" key. In this case, the returned
    number of pods is 0.
    """
    deployment = Deployment(
        key="proc-pb-test-20230511-00000-script",
        kind="helm",
        args={
            "chart": "script",
            "values": {"env": [], "image": "some-image:0.1.0"},
        },
    )
    with _generate_helm_command_args("test-id", deployment) as args:
        template_string = helm_invoke("template", *args)

    num_pods = num_pods_in_deployment("test-id", template_string)
    assert num_pods == 0


def test_num_pods_in_deployment_fake_replica(set_up_helm):
    """
    Test what happens if the helm template contains the
    word "replica" in places other than what we need.
    These should be ignored.
    """
    deployment = Deployment(
        key="proc-pb-replicas-20230511-00000-script",
        kind="helm",
        args={
            "chart": "dask",
            "values": {
                "image": "replicas-image:0.1.0",
                "worker": {"replicas": 1},
            },
        },
    )
    with _generate_helm_command_args("test-id", deployment) as args:
        template_string = helm_invoke("template", *args)

    num_pods = num_pods_in_deployment("test-id", template_string)
    assert num_pods == 2  # 1 worker 1 scheduler


def test_num_pods_in_deployment_visrec_chart(set_up_helm):
    """
    Test with the vis-receive chart, which no longer requires
    any input values information.

    By default, it will deploy a service and single pod,
    so the expected pod number is 1.
    """
    deployment = Deployment(
        key="proc-pb-20230511-00000-script",
        kind="helm",
        args={"chart": "vis-receive"},
    )
    with _generate_helm_command_args("test-id", deployment) as args:
        template_string = helm_invoke("template", *args)

    num_pods = num_pods_in_deployment("test-id", template_string)
    assert num_pods == 1  # 1 worker 1 scheduler


@pytest.mark.timeout(2)
def test_create_deploy_state():
    """
    Deployment state is created with num_pod key.
    """
    config = Config(backend="memory")
    config.backend.delete("/deploy", must_exist=False, recursive=True)

    dpl_id = "test-dep"
    dpl = Deployment(
        key=dpl_id,
        kind="helm",
        args={"chart": "test", "values": {"test": "test"}},
    )

    for watcher in config.watcher():
        for txn in watcher.txn():
            txn.deployment.create(dpl)
            dpl_state = txn.deployment.state(dpl_id).get()

        assert dpl_state is None

        for txn in watcher.txn():
            _create_deploy_state(txn, dpl_id, 4)

        for txn in watcher.txn():
            dpl_state = txn.deployment.state(dpl_id).get()

        assert dpl_state == {"num_pod": 4}

    config.backend.delete("/deploy", must_exist=False, recursive=True)
