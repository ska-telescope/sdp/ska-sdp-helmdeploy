"""
Monitor deployments in the Helm deployer namespace.

Use the Python Kubernetes API to monitor deployments and write status
information into the deployment state entries in the configuration database.
"""

import functools
import json
import logging
import os
import threading
import time
from copy import deepcopy
from typing import Callable

from kubernetes import client as k8s_client
from kubernetes import config as k8s_config
from kubernetes import watch as k8s_watch
from ska_sdp_config import Config
from ska_sdp_config.config import Transaction

LOG = logging.getLogger(__name__)

ERROR_RETRY_PERIOD = int(
    os.environ.get("HELMDEPLOY_MONITORING_ERROR_RETRY_PERIOD", "5")
)
NO_NAD_FOUND_RETRY_PERIOD_SEC = 5 * 60
K8S_DEBUG = os.environ.get("K8S_DEBUG", "0") == "1"
if not K8S_DEBUG:
    logging.getLogger("kubernetes").setLevel(logging.WARNING)

K8S_CONFIGURED = True
try:
    k8s_config.load_incluster_config()
except k8s_config.ConfigException:
    try:
        k8s_config.load_kube_config()
    except k8s_config.ConfigException:
        K8S_CONFIGURED = False

ADDED = "ADDED"
MODIFIED = "MODIFIED"
PENDING = "PENDING"
RUNNING = "RUNNING"
FAILED = "FAILED"
UNKNOWN = "UNKNOWN"
UNSET = "UNSET"
DELETED = "DELETED"
CANCELLED = "CANCELLED"
SUCCEEDED = "SUCCEEDED"
FINISHED = "FINISHED"


def _k8s_api_client():
    config = k8s_client.Configuration.get_default_copy()
    config.debug = K8S_DEBUG
    return k8s_client.ApiClient(configuration=config)


def monitoring_function(func):
    """
    Prepares and injects new Config and ApiClient objects to be used by a
    monitoring thread, ensuring all resources are correctly released after
    the thread has finished its work.
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        config = kwargs.pop("config") if "config" in kwargs else Config()
        api_client = (
            kwargs.pop("api_client")
            if "api_client" in kwargs
            else _k8s_api_client()
        )
        with config, api_client:
            func(config, api_client, *args, **kwargs)

    return wrapper


EventProcessor = Callable[[Config, dict], None]
"""Type hint for functions that process individual monitoring events."""


def _monitor_and_process_events(
    config: Config,
    monitoring_subject: str,
    process_event: EventProcessor,
    k8s_function: Callable,
    **k8s_function_kwargs,
) -> None:
    LOG.debug("%s monitoring started", monitoring_subject)
    watch = k8s_watch.Watch()
    while True:
        for event in watch.stream(k8s_function, **k8s_function_kwargs):
            process_event(config, event)


def _add_error_info(k8s_object: dict, pod_state: str) -> tuple[dict, str]:
    """
    Obtain any failure information from the k8s pod event.

    :param k8s_object: k8s event data dictionary
    :param pod_state: current state of the k8s pod in
            deployment status format

    :return: dict of error_data and updated pod_state
    """
    error_data = {}
    if pod_state in [PENDING, FAILED]:
        container_statuses = k8s_object["status"]["container_statuses"]
        if container_statuses:

            for cstatus in container_statuses:
                container_state = cstatus["state"]

                terminated_state = container_state.get("terminated")
                if terminated_state:
                    error_data.update(
                        {
                            "container_name": cstatus["name"],
                            "state": "terminated",
                            "reason": terminated_state["reason"],
                            "exit_code": terminated_state["exit_code"],
                        }
                    )
                    continue

                waiting_state = container_state.get("waiting")
                if (
                    waiting_state
                    and waiting_state["reason"] != "ContainerCreating"
                ):
                    error_data.update(
                        {
                            "container_name": cstatus["name"],
                            "state": "waiting",
                            "reason": waiting_state["reason"],
                            "message": waiting_state.get("message", ""),
                        }
                    )

                    if pod_state == PENDING:
                        pod_state = FAILED
    return error_data, pod_state


def _get_pod_state(event_type: str, k8s_object: dict, pod_name: str) -> str:
    """
    Return status of pod matched to an SDP deployment status.

    :param event_type: type of k8s event
    :param k8s_object: k8s event data dictionary
    :param pod_name: k8s pod name whose event is being processed

    :return: pod_state
    """
    if event_type in [ADDED, MODIFIED]:
        pod_phase = k8s_object["status"]["phase"].upper()
        LOG.debug("event_type=%s - pod_phase=%s", event_type, pod_phase)
        if pod_phase in [PENDING, FAILED]:
            pod_state = pod_phase

        elif pod_phase == UNKNOWN:
            pod_state = UNSET

        elif pod_phase == SUCCEEDED:
            pod_state = FINISHED

        elif pod_phase == RUNNING:
            if any(
                condition["status"] == "True"
                for condition in k8s_object["status"]["conditions"]
                if condition["type"] == "Ready"
            ):
                pod_state = RUNNING

            else:
                pod_state = PENDING
        else:
            LOG.debug("Pod %s: Unknown pod_phase %s", pod_name, pod_phase)
            pod_state = UNSET

    elif event_type == DELETED:
        pod_state = CANCELLED
        LOG.debug("Pod %s deleted", pod_name)
    elif event_type == FAILED:
        pod_state = event_type
    else:
        LOG.debug(
            "%s - Unexpected Pod Event - K8S type %s", pod_name, event_type
        )
        pod_state = UNSET
    LOG.debug("Pod %s State now %s", pod_name, pod_state)
    return pod_state


def _create_update_dpl_state(
    txn: Transaction,
    deploy_id: str,
    error_data: dict,
    pod_name: str,
    pod_state: str,
) -> None:
    """
    Create or update deployment state with monitoring information,
    if deployment still exists.

    :param txn: config db Transaction
    :param deploy_id: id of the deployment (from config db)
    :param error_data: dict containing k8s event error information
    :param pod_name: name of the pod associated with the deployment
    :param pod_state: state of the pod associated with the deployment
    """
    # Create or update deployment state with pod state
    if txn.deployment.get(deploy_id) is None:
        LOG.debug(
            "Deployment with id %s no longer exists. "
            "Not creating its state.",
            deploy_id,
        )
        return

    LOG.debug("Updating deployment state for id %s", deploy_id)
    dpl_state = txn.deployment.state(deploy_id).get()
    if dpl_state is None:
        dpl_state = {
            "pods": {pod_name: {"status": pod_state}},
        }
        if error_data:
            dpl_state["pods"][pod_name]["error_state"] = error_data
        txn.deployment.state(deploy_id).create(dpl_state)

    else:
        new_state = deepcopy(dpl_state)
        if "pods" in new_state:
            new_state["pods"][pod_name] = {"status": pod_state}
        else:
            new_state["pods"] = {pod_name: {"status": pod_state}}
        if error_data:
            new_state["pods"][pod_name]["error_state"] = error_data

        if new_state != dpl_state:
            txn.deployment.state(deploy_id).update(new_state)


# pylint: disable-next=too-many-branches
def get_pod_helm_name(k8s_pod):
    """
    Get Helm chart name associated with a Pod

    This uses the Kubernetes  'owner_references' field to work out the ultimate
    parent of the Pod given as the argument by working up the Kubernetes
    hierarchy.
    This parent object is verified to have been created by Helm and the Helm
    deployment name is returned

    :param k8s_pod: A Kubernetes Pod object
    :return: str Name of the Helm chart
             or a blank string if not created by Helm
             Note: also returns blank if in the meantime
             the chart was uninstalled
    """
    apps_v1 = k8s_client.AppsV1Api()
    batch_v1 = k8s_client.BatchV1Api()
    namespace = k8s_pod.metadata.namespace
    owner = k8s_pod.metadata.owner_references
    current_obj = k8s_pod
    pod_name = k8s_pod.metadata.name
    LOG.debug("Searching for Helm chart associated with pod %s", pod_name)

    while owner:
        # Trace through K8S hierarchy
        if len(owner) != 1:
            LOG.debug(
                "owner_references expected to be 1 - actual %i", len(owner)
            )
        owner_kind = owner[0].kind
        owner_name = owner[0].name
        LOG.debug(
            "Pod %s hierarchy - type/name = %s/%s",
            pod_name,
            owner_kind,
            owner_name,
        )

        result = None
        if owner_kind == "ReplicaSet":
            result = apps_v1.list_namespaced_replica_set(namespace=namespace)
        elif owner_kind == "Deployment":
            result = apps_v1.list_namespaced_deployment(namespace=namespace)
        elif owner_kind == "StatefulSet":
            result = apps_v1.list_namespaced_stateful_set(namespace=namespace)
        elif owner_kind == "DaemonSet":
            result = apps_v1.list_namespaced_daemon_set(namespace=namespace)
        elif owner_kind == "Job":
            result = batch_v1.list_namespaced_job(namespace=namespace)

        if not result or len(result.items) == 0:
            # K8S hierarchy trace failed.
            # Pod may have been removed, helm chart uninstalled
            current_obj = None
            owner = None

        else:
            for item in result.items:
                if item.metadata.name == owner_name:
                    current_obj = item
                    # owner_references should be None if we reached the top
                    owner = item.metadata.owner_references
                    break

                # Unexpected situation! Parent not found
                # Pod may have been removed in the meantime,
                # helm chart uninstalled
                current_obj = None

            if current_obj is None:
                owner = None

    # current_obj is now the top-level object of the Pod hierarchy

    if current_obj is None:
        LOG.debug("Failed to trace Pod hierarchy - No Helm name found")
        return ""

    labels = current_obj.metadata.labels
    deployment = ""
    if (
        labels
        and "app.kubernetes.io/managed-by" in labels.keys()  # noqa: W503
        and labels["app.kubernetes.io/managed-by"] == "Helm"  # noqa: W503
    ):
        deployment = current_obj.metadata.annotations[
            "meta.helm.sh/release-name"
        ]
    else:
        LOG.debug("Pod manager %s not from Helm", current_obj.metadata.name)

    return deployment


def process_k8s_pod_event(config, event):
    """
    Process Pod change on Kubernetes workspace to deployment states

    :param config: configuration database client
    :param event: Kubernetes API Pod watch event from namespace Pod changes
    """
    event_type = event["type"]
    k8s_object = event["object"].to_dict()
    pod_name = k8s_object["metadata"]["name"]

    helm_name = get_pod_helm_name(event["object"])
    if not helm_name:
        LOG.debug(
            "No matching helm chart found for pod %s, "
            "skipping from monitoring.",
            pod_name,
        )
        return

    deploy_id = None
    for txn in config.txn():
        # List deployment IDs
        deploy_ids = txn.deployment.list_keys()

        # Match pod name to deployment ID - there won't be a match if the
        # deployment has just been deleted and Helm is uninstalling
        # the chart release
        for dpl in deploy_ids:
            if helm_name == dpl:
                deploy_id = dpl
                break

        if deploy_id is None:
            LOG.debug(
                "Pod %s (Helm name %s) not matched to any deployment, "
                "skipping from monitoring",
                pod_name,
                helm_name,
            )
            return

    LOG.info(
        "Processing k8s event type %s for pod %s (Helm name %s) ",
        event_type,
        pod_name,
        helm_name,
    )

    pod_state = _get_pod_state(event_type, k8s_object, pod_name)
    error_data, pod_state = _add_error_info(k8s_object, pod_state)

    for txn in config.txn():
        _create_update_dpl_state(
            txn, deploy_id, error_data, pod_name, pod_state
        )


def check_current_namespace_state(
    config: Config, core_api_v1: k8s_client.CoreV1Api, namespace: str
):
    """
    Pod startup code to check that the deployment states are correct.
    This is in case events may have got lost in any restart.

    :param config: configuration database client
    :param core_api_v1: Instance of the CoreV1 API
    :param namespace: namespace to monitor
    """
    LOG.info("Startup check on namespace %s", namespace)
    event = {"type": MODIFIED, "object": None}
    pods = core_api_v1.list_namespaced_pod(namespace=namespace)
    for pod in pods.items:
        LOG.debug("Startup check on existing Pod %s", pod.metadata.name)
        event["object"] = pod
        # The event here is not from the watcher  - but the K8S object
        # and its processing are the same!
        process_k8s_pod_event(config, event)


@monitoring_function
def monitor_pods(
    config: Config, api_client: k8s_client.ApiClient, namespace: str
):
    """
    Monitor pods in the Helm deployer namespace.

    :param config: configuration database client
    :param api_client: Kubernetes API client
    :param namespace: namespace to monitor

    """
    core_v1_api = k8s_client.CoreV1Api(api_client)
    # Perform a check on any existing Pods in the namespace in case
    # events may have been mislaid
    # It should not matter if the event watcher also processes
    # the same Pod!
    check_current_namespace_state(config, core_v1_api, namespace)
    while True:
        try:
            _monitor_and_process_events(
                config,
                "Pod",
                process_k8s_pod_event,
                core_v1_api.list_namespaced_pod,
                namespace=namespace,
                timeout_seconds=0,
            )
        except Exception:  # pylint: disable=broad-exception-caught
            LOG.exception(
                "Unexpected error, will retry in %d seconds",
                ERROR_RETRY_PERIOD,
            )
            time.sleep(ERROR_RETRY_PERIOD)


def _to_sdp_resources(
    network_attach_def_metadata: dict,
) -> tuple[dict, dict] | None:
    name = network_attach_def_metadata["name"]
    namespace = network_attach_def_metadata["namespace"]
    annotations = network_attach_def_metadata.get("annotations", None)
    if not annotations:
        LOG.warning(
            "No annotations found in NetworkAttachmentDefinition %s/%s,"
            " skipping",
            namespace,
            name,
        )
        return None
    supernet = annotations.get("sdp.skao.int/ip-supernet", None)
    cidr_bits = annotations.get("sdp.skao.int/ip-subnet-cidr-bits", None)
    if supernet is None or cidr_bits is None:
        LOG.warning(
            "Insufficient annotations in NetworkAttachmentDefinition %s/%s,"
            " skipping",
            namespace,
            name,
        )
        return None

    network_attach_def = {
        "name": name,
        "namespace": namespace,
    }
    supernet = {
        "ip-supernet": supernet,
        "ip-subnet-cidr-bits": int(cidr_bits),
    }
    LOG.info(
        "Built network-attachment-definition resource for config DB: %s",
        network_attach_def,
    )
    LOG.info("Built supernet resource for config DB: %s", supernet)
    return network_attach_def, supernet


def process_k8s_network_attach_def_event(config: Config, event: dict) -> None:
    """
    Process NetworkAttachmentDefinition changes on Kubernetes to SDP resources.

    :param config: configuration database client
    :param event: Kubernetes API watch event from CustomObjects changes
    """
    event_type = event["type"]
    kind = event["object"]["kind"]
    LOG.info("Received %s event for object of kind %s", event_type, kind)
    if kind != "NetworkAttachmentDefinition":
        LOG.warning("Object of kind != NetworkAttachmentDefinition, skipping")
        return
    resources = _to_sdp_resources(event["object"]["metadata"])
    if resources is None:
        return
    network_attach_def, supernet = resources
    resource_name = network_attach_def["name"]
    for txn in config.txn():
        for resource_type, value in (
            ("network-attachment-definition", network_attach_def),
            ("supernet", supernet),
        ):
            path = f"/resource/{resource_type}:{resource_name}"
            serialised_value = json.dumps(value)
            existed = txn.raw.get(path) is not None
            if existed:
                txn.raw.update(path, serialised_value)
            else:
                txn.raw.create(path, serialised_value)
            LOG.info(
                "%s %s with new contents",
                "Updated" if existed else "Created",
                path,
            )


def _monitor_custom_objects(
    config: Config,
    api_client: k8s_client.ApiClient,
    monitoring_subject: str,
    process_event: EventProcessor,
    **list_cluster_custom_object_kwargs,
) -> None:
    custom_objects_api = k8s_client.CustomObjectsApi(api_client)
    _monitor_and_process_events(
        config,
        monitoring_subject,
        process_event,
        custom_objects_api.list_cluster_custom_object,
        **list_cluster_custom_object_kwargs,
    )


def _monitor_network_attachment(
    config: Config, api_client: k8s_client.ApiClient
) -> bool:
    """
    Monitor NetworkAttachmentDefinition objects in the Kubernetes cluster.

    :param config: configuration database client
    :param api_client: Kubernetes API client

    :return: True if monitoring should continue,
             False if it should stop
    """
    try:
        _monitor_custom_objects(
            config,
            api_client,
            "NetworkAttachmentDefinitions",
            process_k8s_network_attach_def_event,
            group="k8s.cni.cncf.io",
            plural="network-attachment-definitions",
            version="v1",
            label_selector="sdp.skao.int/available-for-allocation=true",
        )
        return True

    except k8s_client.ApiException as ex:
        match ex.status:
            case 410:
                LOG.warning(
                    "NetworkAttachmentDefinitions Gone (410), "
                    "retrying monitoring in %d seconds",
                    ERROR_RETRY_PERIOD,
                )
                time.sleep(ERROR_RETRY_PERIOD)
                return True
            case 404:
                LOG.warning(
                    "NetworkAttachmentDefinitions Not Found (404), "
                    "retrying monitoring in %d seconds",
                    NO_NAD_FOUND_RETRY_PERIOD_SEC,
                )
                time.sleep(NO_NAD_FOUND_RETRY_PERIOD_SEC)
                return True
            case 403:
                LOG.error(
                    "NetworkAttachmentDefinitions Forbidden (403),"
                    "Ending monitoring due to unauthorized access"
                )
                return False
            case _:
                LOG.exception(
                    "Unexpected Kubernetes API error, "
                    "retrying monitoring in %d seconds",
                    ERROR_RETRY_PERIOD,
                )
                time.sleep(ERROR_RETRY_PERIOD)
                return True

    except Exception:  # pylint: disable=broad-exception-caught
        LOG.exception(
            "Unexpected error, retrying monitoring in %d seconds",
            ERROR_RETRY_PERIOD,
        )
        time.sleep(ERROR_RETRY_PERIOD)
        return True


@monitoring_function
def monitor_network_attach_def(
    config: Config, api_client: k8s_client.ApiClient
) -> None:
    """
    Monitor NetworkAttachmentDefinition objects in the Kubernetes cluster.

    :param config: configuration database client
    :param api_client: Kubernetes API client
    """
    while True:
        if not _monitor_network_attachment(config, api_client):
            break


def start_monitoring(config_backend: str | None, namespace: str):
    """
    Start monitoring threads.

    :param config_backend: backend for the configuration database client
    :param namespace: namespace to monitor

    """
    if K8S_CONFIGURED:
        LOG.info("Starting monitoring threads")
        pod_monitor_thread = threading.Thread(
            target=monitor_pods,
            kwargs={"config": Config(config_backend), "namespace": namespace},
            daemon=True,
            name="Pod monitoring",
        )
        pod_monitor_thread.start()
        network_attach_def_monitor_thread = threading.Thread(
            target=monitor_network_attach_def,
            kwargs={"config": Config(config_backend)},
            daemon=True,
            name="NetworkAttachmentDefinition monitoring",
        )
        network_attach_def_monitor_thread.start()
    else:
        LOG.warning("Kubernetes configuration not detected")
