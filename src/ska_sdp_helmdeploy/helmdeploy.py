"""
Helm deployment controller.

Installs/updates/uninstalls Helm releases depending on information in the SDP
configuration.
"""

import contextlib
import logging
import os
import shutil
import signal
import subprocess
import sys
import tempfile
import time
from pathlib import Path

import ska_sdp_config
import yaml
from dotenv import load_dotenv
from ska_ser_logging import configure_logging

from .monitoring import start_monitoring

load_dotenv()

# Load environment variables

# Helm command
HELM_COMMAND = shutil.which(os.getenv("SDP_HELM_COMMAND", "helm"))
# Timeout for Helm command
HELM_TIMEOUT = int(os.getenv("SDP_HELM_TIMEOUT", "300"))
# Namespace in which to deploy charts
HELM_NAMESPACE = os.getenv("SDP_HELM_NAMESPACE", "sdp")
# Prefix for Helm release names
HELM_RELEASE_PREFIX = os.getenv("SDP_HELM_RELEASE_PREFIX", "")
# Prefix to apply to names of Helm deployer charts
HELM_CHART_PREFIX = os.getenv(
    "SDP_HELM_CHART_PREFIX",
    "ska-sdp-helmdeploy",
)
# URL of Helm deployer chart repository
HELM_CHART_REPO_URL = os.getenv(
    "SDP_HELM_CHART_REPO_URL",
    "https://artefact.skao.int/repository/helm-internal",
)
# Time interval in seconds between updates from Helm chart repositories (0 for
# no updates)
HELM_CHART_REPO_REFRESH = int(os.getenv("SDP_HELM_CHART_REPO_REFRESH", "0"))
# Maximum number of releases to list at once
HELM_LIST_MAX = int(os.getenv("SDP_HELM_LIST_MAX", "256"))

# Time intervals in seconds for idle looping of the Helm deployer as used by
# the Kubernetes liveness probe
LOOP_INTERVAL = int(os.getenv("SDP_HELMDEPLOY_LOOP_INTERVAL", "60"))

# Liveness file name - must match the name used in the Kubernetes
# Pod yaml probe configuration!
LIVENESS_FILE = Path(os.getenv("SDP_HELMDEPLOY_LIVENESS_FILE", "/tmp/alive"))

# Logging level
LOG_LEVEL = os.getenv("SDP_LOG_LEVEL", "DEBUG")

# Name to use for the Helm deployer chart repository
HELM_CHART_REPO_NAME = "helmdeploy"
# Chart repositories to use, as a list of (name, url) pairs - right now this
# uses only the Helm deployer chart repository, but could be extended
HELM_CHART_REPO_LIST = [
    (HELM_CHART_REPO_NAME, HELM_CHART_REPO_URL),
]

# Initialise logger.
configure_logging(level=LOG_LEVEL)
log = logging.getLogger(__name__)


def invoke(*cmd_line):
    """
    Invoke a command with the given command line.

    :returns: output of the command
    :raises: ``subprocess.CalledProcessError`` if command returns
        an error status

    """
    # Perform call. We really want to do this without check=True, as
    # we need to log the stdout before raising an exception
    log.debug(" ".join(["$"] + list(cmd_line)))
    # pylint: disable=subprocess-run-check
    result = subprocess.run(
        cmd_line,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        timeout=HELM_TIMEOUT,
    )
    # Log results
    log.debug("Code: %s", result.returncode)
    stdout = result.stdout.decode()
    stderr = result.stderr.decode()
    for line in stdout.splitlines():
        log.debug("-> %s", line)
    for line in stderr.splitlines():
        log.error("-> %s", line)
    result.check_returncode()

    return stdout


def helm_invoke(*args):
    """
    Invoke Helm with the given command-line arguments.

    :returns: output of the command
    :raises: ``subprocess.CalledProcessError`` if command returns
        an error status

    """
    return invoke(*([HELM_COMMAND] + list(args)))


def release_name(dpl_id):
    """
    Get Helm release name from deployment ID.

    :param dpl_id: deployment ID
    :returns: release name

    """
    if HELM_RELEASE_PREFIX:
        release = f"{HELM_RELEASE_PREFIX}-{dpl_id}"
    else:
        release = dpl_id
    return release


def delete_helm(dpl_id):
    """
    Delete a Helm deployment.

    :param dpl_id: deployment ID

    """
    log.info("Deleting deployment %s...", dpl_id)

    # Try to delete
    try:
        release = release_name(dpl_id)
        helm_invoke("uninstall", release, "-n", HELM_NAMESPACE)
        return True
    except subprocess.CalledProcessError:
        return False  # Assume it was already gone


@contextlib.contextmanager
def _generate_helm_command_args(dpl_id, deploy):
    """
    Context manager to generate arguments for the helm
    template and install commands.

    If values are specified in the deployment request, it creates a
    temporary file containing the values to pass to the commands,
    and this is cleaned up on exit.
    """
    # Get chart name. If it does not contain "/",
    # it is from the Helm deployer chart repository
    chart = deploy.args["chart"]
    if "/" not in chart:
        if HELM_CHART_PREFIX:
            chart = f"{HELM_CHART_REPO_NAME}/{HELM_CHART_PREFIX}-{chart}"
        else:
            chart = f"{HELM_CHART_REPO_NAME}/{chart}"
    version = deploy.args.get("version")
    if version:
        log.debug("Chart: %s version: %s", chart, version)
    else:
        log.debug("Chart: %s", chart)

    # Build command line
    release = release_name(dpl_id)
    args = [release, chart, "-n", HELM_NAMESPACE]

    # Optional Helm chart version string - either exact (n.n.n) or
    # constraint (eg. ^n.n.n)
    if version:
        args.extend(["--version", version])

    # Write any values to a temporary file
    if "values" in deploy.args:
        if (
            "fullnameOverride" not in deploy.args["values"]
            or not deploy.args["values"]["fullnameOverride"]
        ):
            deploy.args["values"]["fullnameOverride"] = release
        values = yaml.dump(deploy.args["values"])

    else:
        values = f'{{"fullnameOverride": "{release}"}}'

    log.debug("Values:")
    for line in values.splitlines():
        log.debug("-> %s", line)
    with tempfile.NamedTemporaryFile(
        mode="w", suffix=".yaml", delete=False
    ) as file:
        file.write(values)
    filename = file.name
    args.extend(["-f", filename])

    yield args

    # Delete values file
    os.unlink(filename)


def num_pods_in_deployment(dpl_id, template_str):
    """
    Determine how many replicas of pods in the deployment
    are expected from template string (generated by
    `helm template`).

    :param dpl_id: deployment ID
    :param template_str: string generated by `helm template`
    :return: number of pods in deployment
    """
    try:
        templates = yaml.safe_load_all(template_str)
    except yaml.YAMLError as err:
        raise yaml.YAMLError(
            f"Failed to load template for deployment with id {dpl_id}."
        ) from err

    pod_num = 0
    for template in templates:
        kind = template.get("kind", None)
        if kind in ("Deployment", "StatefulSet"):
            replicas = template.get("spec", {}).get("replicas", 0)
            pod_num += replicas

    return pod_num


def template_and_create_chart(dpl_id, deploy):
    """
    Run helm template to find number of expected pods in
    deployment, and if the command is successful, create deployment.

    :param dpl_id: deployment ID
    :param deploy: the deployment

    :returns: (success, num_pod)
        success: whether the command was successful or not
        num_pod: number of expected pods in deployment
    """
    log.info("Creating deployment %s...", dpl_id)

    with _generate_helm_command_args(dpl_id, deploy) as args:
        # Try to run helm template
        num_pod = 0
        try:
            output = helm_invoke("template", *args)
            success = True
        except subprocess.CalledProcessError:
            log.error(
                "Failed to run `helm template` for deployment with id %s! "
                "Cannot create deployment.",
                dpl_id,
            )
            success = False
        else:
            num_pod = num_pods_in_deployment(dpl_id, output)

        # if templating succeeds, install chart
        if success:
            try:
                helm_invoke("install", *args)
            except subprocess.CalledProcessError as err:
                log.error(
                    "Deployment with id %s could not be installed. "
                    "\nError: %s",
                    dpl_id,
                    str(err),
                )
                success = False

        return success, num_pod


def update_helm():
    """Refresh Helm chart repositories."""
    try:
        helm_invoke("repo", "update")
    except subprocess.CalledProcessError:
        log.error("Could not refresh chart repositories")


def list_helm():
    """
    List Helm deployments.

    :returns: list of deployment IDs

    """
    # Query helm for chart releases
    args = ["list", "-q", "-n", HELM_NAMESPACE, "--max", str(HELM_LIST_MAX)]
    offset = 0
    releases = []
    while True:
        # List releases starting at offset
        releases_off = helm_invoke(*args, "--offset", str(offset)).splitlines()
        releases += releases_off
        if len(releases_off) < HELM_LIST_MAX:
            break
        offset += HELM_LIST_MAX

    if HELM_RELEASE_PREFIX:
        # Filter releases for those matching deployments
        deploys = [
            release[len(HELM_RELEASE_PREFIX) + 1 :]
            for release in releases
            if release.startswith(HELM_RELEASE_PREFIX + "-")
        ]
    else:
        deploys = releases
    return deploys


def _get_deployment(txn, dpl_id):
    try:
        return txn.deployment.get(dpl_id)
    except ValueError as error:
        log.warning("Deployment %s failed validation: %s!", dpl_id, str(error))
    return None


def _create_deploy_state(txn, dpl_id, num_pod):
    """
    Create state of deployment with information
    about number of expected pods
    """
    dpl_state = txn.deployment.state(dpl_id).get()
    if dpl_state is None:
        dpl_state = {"num_pod": num_pod}
        txn.deployment.state(dpl_id).create(dpl_state)
        return

    if "num_pod" not in dpl_state:
        dpl_state["num_pod"] = num_pod
        txn.deployment.state(dpl_id).update(dpl_state)
        return


# pylint: disable=too-many-branches,too-many-locals
def main_loop(config, backend):
    """
    Main loop of Helm controller.

    :param config: configuration db client
    :param backend: for configuration database

    """
    for txn in config.txn():
        txn.self.ownership.take()

    # Configure Helm repositories
    for name, url in HELM_CHART_REPO_LIST:
        helm_invoke("repo", "add", name, url)

    # Refresh time of zero means no chart refresh
    if HELM_CHART_REPO_REFRESH != 0:
        next_chart_refresh = time.time() + HELM_CHART_REPO_REFRESH
        log.debug(
            "Helm charts will be updated every %d seconds",
            HELM_CHART_REPO_REFRESH,
        )
    else:
        log.debug("Helm chart updating disabled")

    # Start monitoring of deployments
    start_monitoring(backend, HELM_NAMESPACE)

    # Set Helm deployer main loop timer
    timeout = LOOP_INTERVAL
    log.debug("Main loop timeout is %d seconds", timeout)

    # Wait for something to happen
    for watcher in config.watcher(timeout=timeout):
        # Update Liveness file
        LIVENESS_FILE.touch()

        # Check the connection lease.
        for txn in watcher.txn():
            txn.self.take_ownership_if_not_alive()

        # Refresh charts?
        if HELM_CHART_REPO_REFRESH and time.time() > next_chart_refresh:
            update_helm()
            next_chart_refresh = time.time() + HELM_CHART_REPO_REFRESH

        # List deployments
        for txn in watcher.txn():
            target_deploys = txn.deployment.list_keys()
        deploys = list_helm()

        # Check for deployments we should delete
        for dpl_id in deploys:
            if dpl_id not in target_deploys:
                # Delete it
                delete_helm(dpl_id)

        # Check for deployments we should add
        for dpl_id in target_deploys:
            if dpl_id not in deploys:
                # Get details
                for txn in watcher.txn():
                    deploy = _get_deployment(txn, dpl_id)

                    # If vanished or wrong kind, ignore
                    if deploy is None or deploy.kind != "helm":
                        continue

                    try:
                        # Create deployment and get number of pods
                        success, num_pod = template_and_create_chart(
                            dpl_id, deploy
                        )
                    except yaml.YAMLError as err:
                        log.error(
                            "`helm template` failed for deployment %s "
                            "with the following error:\n %s \n"
                            "Helm chart cannot be installed.",
                            dpl_id,
                            str(err),
                        )
                        continue

                    if not success:
                        # if helm chart couldn't be installed
                        continue

                    # Create its state and update it with the
                    # number of pods expected in the deployment
                    _create_deploy_state(txn, dpl_id, num_pod)


def terminate(_signame, _frame):
    """Terminate the program."""
    log.info("Asked to terminate")
    sys.exit(0)


def main(backend=None):
    """Main."""
    signal.signal(signal.SIGTERM, terminate)
    config = ska_sdp_config.Config(
        backend=backend, owned_entity=("component", "helmdeploy")
    )
    try:
        main_loop(config, backend)
    finally:
        # Close configuration DB connection
        config.close()
