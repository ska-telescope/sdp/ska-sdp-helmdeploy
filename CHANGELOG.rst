Changelog
=========

1.0.0
-----

-  Updated dependencies for list, see pyproject.toml on the MR
   (`MR86 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/86>`__)
-  Use SKA theme in documentation
   (`MR85 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/85>`__)
-  Remove broken links and update Poetry versions
   (`MR83 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/83>`__)
-  Match k8s pods to helm charts for monitoring
   (`MR84 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/84>`__)
-  Update Dockerfile to use SKA Python base image
   (`MR80 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/80>`__)

0.17.1
------

-  Use ska-sdp-config 0.10.2
   (`MR82 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/82>`__)

   -  Fixes SKB-685: txn.update is only committed if there is a
      difference between existing and new value

-  Fixes SKB-685: Monitoring only updates deployment state if it differs
   from existing state
   (`MR79 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/79>`__)

.. _section-1:

0.17.0
------

-  Use ska-sdp-config 0.10.1
   (`MR78 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/78>`__)
-  Log errors if ``helm`` command fails
   (`MR78 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/78>`__)
-  Capture Pod error data and add to Config DB deployment/state
   (`MR76 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/76>`__)

.. _section-2:

0.16.1
------

-  Bugfix in monitoring thread wrapper
   (`MR74 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/74>`__)

.. _section-3:

0.16.0
------

-  Update ska-sdp-config to 0.9.0 to adapt changes in pydantic models
   (`MR73 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/73>`__).
-  Switched to use ska-sdp-python base image
   (`MR72 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/72>`__)
-  Fixed issue of stacktraces being logged on 410 k8s API errors
   (`MR68 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/68>`__)

.. _section-4:

0.15.0
------

-  Update ska-sdp-config to 0.6.0 and port code to use new API
   (`MR65 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/65>`__).

.. _section-5:

0.14.2
------

-  Update ska-sdp-config to 0.5.6 and close connection to Configuration
   DB upon exit
   (`MR61 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/61>`__).
-  Ensure background monitoring threads stay alive and continue running
   even in the face of unexpected exceptions
   (`MR-58 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/58>`__).

.. _section-6:

0.14.1
------

-  New responsibility of keeping track of platform-level resources
   defined as Kubernetes resources, These k8s resources need to be
   therefore monitored, and will result in entries being added/updated
   to the SDP config DB. Working towards
   (`ADR-90 <https://confluence.skatelescope.org/display/SWSI/ADR-90+SDP+processing+resource+management?searchId=1Y86AXDG3>`__)
   (`MR53 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/53>`__)

.. _section-7:

0.14.0
------

-  Fix listing of releases when there more than 256 installed by using
   pagination; add ``SDP_HELM_LIST_MAX`` environment variable to control
   the number listed in one ``helm list`` call
   (`MR51 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/51>`__)

.. _section-8:

0.13.1
------

-  Updated to use the new version of config DB 0.5.3
   (`MR50 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/50>`__)

.. _section-9:

0.13.0
------

-  Updated to use the new version of config DB 0.5.2
   (`MR49 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/49>`__)

.. _section-10:

0.12.3
------

-  Monitoring is now based on pod phases and pod conditions instead of
   container statuses
   (`MR48 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/48>`__)
-  Updated RTD build to use python3.11
   (`MR46 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/46>`__)
-  Added helm chart versioning support
   (`MR45 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/45>`__)

.. _section-11:

0.12.2
------

-  Handle helmdeploy pod restarts when monitoring pods
   (`MR44 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/44>`__)
-  Set timeout on pod monitoring to 0 (k8s watch.stream)
   (`MR44 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/44>`__)
-  Improve pod monitoring logic
   (`MR44 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/44>`__)

.. _section-12:

0.12.1
------

-  Add error handling when ``helm install`` is invoked
   (`MR43 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/43>`__)

.. _section-13:

0.12.0
------

-  Determine the number of pods that a deployment is expected to have
   and update the deployment state with the information
   (`MR38 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/38>`__)
-  Pod monitoring is now more detailed and takes into account the
   statuses of individual containers within a pod
   (`MR36 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/36>`__,
   `MR37 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/37>`__,
   `MR41 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/41>`__)
-  Poetry install in Dockerfile
   (`MR40 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/40>`__)
-  Bugfix: Helm Deployer did not clean up deleted deployments properly
   (`MR37 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/37>`__)

.. _section-14:

0.11.4
------

-  ska-sdp-config == 0.4.4
   (`MR35 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/35>`__)

.. _section-15:

0.11.3
------

-  Add exit handler and updated component to generate lease entry
   (alive_key)
   (`MR33 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/33>`__)
-  Add idle looping with updating of flag file to integrate with
   Kubernetes liveness probing
   (`MR34 <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy/-/merge_requests/34>`__)

.. _section-16:

0.9.1
-----

-  Publish Docker image in central artefact repository.

.. _section-17:

0.9.0
-----

-  Modify main loop to use a configuration watcher.
-  Improve handling of temporary YAML files for values.
