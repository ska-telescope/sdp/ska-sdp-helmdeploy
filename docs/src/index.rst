SDP Helm Deployer
=================

The Helm Deployer is a component of the SDP, which is responsible
for deploying helm charts requested by processing scripts.

It continuously monitors the configuration database for deployment
entries, which will tell the Helm deployer whether a release needs
to be installed, updated, or uninstalled.

It has direct access to a processing namespace, which does not
hold the SDP control systems, but instead is used for deploying
and running processing scripts.


Kubernetes event monitoring
---------------------------

The Helm Deployer has been set up to continuously monitor Kubernetes
events. This feature uses the ``kubernetes`` python API
and runs using threading.


Pods
^^^^

Pod monitoring focuses on the deployments
the Helm Deployer is responsible for.
It updates (or creates if it does not exist) the deployment state entry in
the configuration database, and adds information about the status of each
pod that belongs to the specific deployment. The pod statuses it provides are:

.. code-block::

    PENDING
    RUNNING
    FINISHED
    CANCELLED
    FAILED
    UNSET

The values directly depend on the pod phase and its condition event values.
A pod is set to ``RUNNING`` only if its phase is ``RUNNING`` and if at least
one of its conditions has type ``READY`` and status ``True``.


SDP resources
^^^^^^^^^^^^^

The Helm Deployer also monitors certain Kubernetes ``CustomObject``\ s
that represent platform-level resources that SDP needs to be aware of.
These resources are read from Kubernetes
and result in entries in the configuration database
under the ``/resource`` prefix.

Currently, the following Kubernetes resources are monitored:

 * ``NetworkAttachmentDefinition``\ s
   (Kubernetes plural: ``network-attachment-definitions``)
   with a label ``sdp.skao.int/available-for-allocation`` set to ``true``.
   These result
   in ``network-attachment-definition`` and ``supernet`` SDP resources
   in the configuration database.
   Together they define the parameters needed
   by the :doc:`vis-receive script <ska-sdp-script:scripts/vis-receive>`
   to coordinate the setup of the receiver pods
   to directly listen for data sent by CBF.
