include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/dependencies.mk

PROJECT_NAME = ska-sdp-helmdeploy
PROJECT_PATH = ska-telescope/sdp/ska-sdp-helmdeploy
ARTEFACT_TYPE = oci

# W503 line break before binary operator - not compatible with black
# E203 whitespace before ':' - not compatible with black
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=W503,E203

CHANGELOG_FILE = CHANGELOG.rst