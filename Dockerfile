FROM artefact.skao.int/ska-build-python:0.1.1 AS build

ARG HELM_VERSION=3.12.0

WORKDIR /build
RUN curl https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar xz && \
    install linux-amd64/helm /usr/local/bin/helm && \
    rm -r linux-amd64

COPY . ./

ENV POETRY_VIRTUALENVS_CREATE=false

RUN poetry install --no-root --only main
RUN pip install --no-compile --no-dependencies .

FROM artefact.skao.int/ska-python:0.1.2

WORKDIR /app

COPY --from=build /usr/local/ /usr/local/

ENTRYPOINT ["ska-sdp-helmdeploy"]
